# Portuguese translations for Hue.
# Copyright (C) 2012 Cloudera, Inc
# This file is distributed under the same license as the Hue project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Hue VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-10-17 16:28-0400\n"
"PO-Revision-Date: 2012-11-07 13:08-0800\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: pt <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 0.9.6\n"

#: src/liboozie/conf.py:31
msgid ""
"URL of Oozie server. This is required for job submission. Empty value "
"disables the config check."
msgstr ""
"URL do servidor Oozie. Necessário para o envio do job. Ausência de valor "
"desactiva a verificação de config."

#: src/liboozie/conf.py:37
msgid "Whether Oozie requires client to perform Kerberos authentication."
msgstr "Se o Oozie requer que o cliente efectue a autenticação Kerberos ou não."

#: src/liboozie/conf.py:44
msgid ""
"Location on HDFS where the workflows/coordinators are deployed when "
"submitted by a non-owner. Parameters are $TIME, $USER and $JOBID, e.g. "
"/user/$USER/hue/deployments/$JOBID-$TIME"
msgstr ""
"Localização no HDFS onde os workflows/coordinators são implementados "
"quando enviados por um não proprietário. Os parâmetros são $TIME, $USER e"
" $JOBID, por exemplo /user/$USER/hue/deployments/$JOBID-$TIME"

#: src/liboozie/conf.py:55
msgid ""
"Whether Hue append jar paths to the oozie.libpath instead of copying them"
" into the workspace. This makes submissions faster and less prone to HDFS"
" permission errors"
msgstr ""

#: src/liboozie/conf.py:90
msgid "The Oozie server is not available"
msgstr "O servidor Oozie não está disponível"

#: src/liboozie/conf.py:106
#, fuzzy
msgid "Oozie Share Lib path is not available"
msgstr "O servidor Oozie não está disponível"

#: src/liboozie/conf.py:115
msgid "Oozie Share Lib not installed in default location."
msgstr "Oozie Share Lib não instalada na localização predefinida."

#: src/liboozie/submission2.py:44 src/liboozie/submittion.py:43
#, python-format
msgid "Submission already submitted (Oozie job id %s)"
msgstr "Submissão já enviada (ID do trabalho do Oozie %s)"

#: src/liboozie/submission2.py:181 src/liboozie/submittion.py:151
#, python-format
msgid "Failed to create deployment directory: %s"
msgstr "Falha ao criar o directório de implementação: %s"

#: src/liboozie/submission2.py:311 src/liboozie/submittion.py:252
#, python-format
msgid "Path is not a directory: %s."
msgstr "O caminho não é um directório: %s."

#: src/liboozie/submission2.py:316 src/liboozie/submittion.py:257
#, python-format
msgid "Error accessing directory '%s': %s."
msgstr "Erro ao aceder a directório \"%s\": %s."

#: src/liboozie/types.py:381
msgid "Insufficient permission."
msgstr "Permissão insuficiente."

#: src/liboozie/types.py:382
#, python-format
msgid "Permission denied. User %(username)s cannot modify user %(user)s's job."
msgstr ""
"Permissão recusada. O utilizador %(username)s não pode modificar um "
"trabalho %(user)s do utilizador."

